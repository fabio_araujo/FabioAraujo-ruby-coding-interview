class Tweet < ApplicationRecord
  ##
  # Relations
  belongs_to :user

  ##
  # Scope
  scope :newest, -> { order(created_at: :desc) }
  scope :by_user_id, -> (user_id) { where(user_id: user_id) }
end
