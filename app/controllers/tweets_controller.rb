class TweetsController < ApplicationController
  def index
    tweets  = Tweet.all
    user_id = params[:user_id]
    tweets  = tweets.by_user_id(user_id) if user_id.present?

    # Pagination logic
    page              = params[:page].to_i
    page              = 1 if page == 0
    quantity_per_page = 10
    offset            = page == 1 ? 0 : page * quantity_per_page

    render json: tweets.limit(quantity_per_page).offset(offset)
  end
end
