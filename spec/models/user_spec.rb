require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Scopes' do
    describe '.by_username' do
      let(:user) { create(:user, username: 'hello') }

      it 'returns the partial match' do
        expect(described_class.by_username('he')).to include(user)
      end

      context 'bad path 👎' do
        it 'returns the partial match' do
          expect(described_class.by_username('xs')).not_to include(user)
        end
      end
    end
  end
end
